/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mac
 */
public class CPUTest {
    
    public CPUTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of toString method, of class CPU.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        CPU instance = new CPU();
        instance.setId(1);
        instance.setModel("Modelo");
        instance.setName("CPU");
        String expResult = "CPU: " + "id: " + "1" + " ; " + "name: " + "CPU" + "model :" + "Modelo";
        String result = instance.toString();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getArea method, of class CPU.
     */
    @Test
    public void testGetArea() {
        System.out.println("getArea");
        CPU instance = new CPU();
        double expResult = 0.0;
        double result = instance.getArea();
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of getTemp_max method, of class CPU.
     */
    @Test
    public void testGetTemp_max() {
        System.out.println("getTemp_max");
        CPU instance = new CPU();
        double expResult = 0.0;
        double result = instance.getTemp_max();
        assertEquals(expResult, result, 0.0);
       
    }
    
}
