/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.model;

import java.util.ArrayList;
import java.util.List;
import static junit.framework.Assert.assertEquals;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Pedro Gomes <1130383@isep.ipp.pt>
 */
public class ProjectTest {

	public ProjectTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of setConnectionsList method, of class Project.
	 */
	@Test
	public void testSetConnectionsList() {
		System.out.println("setConnectionsList");

		//int[] values = new int[6];
		boolean flag = false;
		boolean flag_expected = true;

		Project instance = new Project();

		/**
		 * Components.
		 */
		CPU m_cpu = new CPU();
		Motherboard m_mb = new Motherboard();
		Cooler m_cooler = new Cooler();
		Tower m_tower = new Tower();

		List<Component> comp = new ArrayList<>();
		List<Connection> conn = new ArrayList<>();

		/**
		 * Components added to the list.
		 */
		comp.add(m_cpu);
		comp.add(m_mb);
		comp.add(m_cooler);
		comp.add(m_tower);

		/**
		 * vai dar call to setConnectionsList.
		 */
		instance.setComponentsList(comp);

		conn = instance.getConnectionsList();

		for (Connection m_conn : conn) {

			System.out.println(m_conn.toString());
			System.out.println("");
		}

		for (Connection m_conn : conn) {
			/**
			 * test first connection;
			 */
			if (m_conn.getComponentA().getIndex() == 1 && m_conn.getComponentB().
				getIndex() == 2) {
				flag = true;
			}
			break;
		}
		assertEquals(flag, flag_expected);
	}

}
