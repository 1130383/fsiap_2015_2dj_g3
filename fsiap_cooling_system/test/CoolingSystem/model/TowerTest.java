/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mac
 */
public class TowerTest {
    
    public TowerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   
    /**
     * Test of toString method, of class Tower.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Tower instance = new Tower();
        instance.setId(1);
        instance.setModel("Modelo");
        instance.setName("Tower");
        String expResult = "Tower : " + "id: " + "1" + " ; " + "name: " + "Tower" + "model :" + "Modelo";;
        String result = instance.toString();
        assertEquals(expResult, result);
        
    }

 
}
