/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.model;

import java.text.DecimalFormat;
import static junit.framework.Assert.assertEquals;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Pedro Gomes <1130383@isep.ipp.pt>
 */
public class ConnectionTest {

	public ConnectionTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	/**
	 * Test of transfer method, of class Connection.
	 */
	@Test
	public void testTransfer() {

		DecimalFormat df = new DecimalFormat("0000E0");

		System.out.println("");
		System.out.println("transfer");
		System.out.println("");

		/**
		 * in seconds.
		 */
		double timeInterval = 1;

		/**
		 * 0.5 m2 área.
		 */
		CPU m_cpu = new CPU(0.05);
		m_cpu.setInstantUsage(80);
		Motherboard m_mb = new Motherboard(0.5);

		Tower m_tower = new Tower();

		Connection instance = new Connection("cpu_to_mb", m_cpu, m_mb);
		Connection instance_x = new Connection("cpu_to_tower", m_cpu, m_tower);

		double result = instance.transfer(timeInterval);
		double result_x = instance_x.transfer(timeInterval);

		System.out.println("Resultado: " + df.format(result));

		System.out.println("Resultado: " + df.format(result_x));

		double expResult = 200500.0;

		assertEquals(expResult, result);

	}

}
