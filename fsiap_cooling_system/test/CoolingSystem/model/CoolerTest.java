/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mac
 */
public class CoolerTest {
    
    public CoolerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

   

    /**
     * Test of toString method, of class Cooler.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Cooler instance = new Cooler();
        instance.setModel("Modelo");
        instance.setId(1);
        instance.setName("Cooler");
        String expResult = "Cooler : " + "id: " + "1" + " ; " + "name: " + "Cooler" + "model :" + "Modelo";;
        String result = instance.toString();
        assertEquals(expResult, result);
        
    }
    
}
