/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mac
 */
public class MotherboardTest {
    
    public MotherboardTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of toString method, of class Motherboard.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Motherboard instance = new Motherboard();
        instance.setModel("Modelo");
        instance.setId(1);
        instance.setName("Motherboard");
        String expResult = "Motherboard: " + "id: " + "1" + " ; " + "name: " + "Motherboard" + "model :" + "Modelo";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    
}
