/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.Resources;

import CoolingSystem.model.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Renato
 */
public class ImportCSVTest {
    
    public ImportCSVTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of importComponents method, of class ImportCSV.
     * @throws java.lang.Exception
     */
    @Test
    public void testImportComponents() throws Exception {
        System.out.println("importComponents");
        File f = new File("fsiap_teste.csv");
        ImportCSV instance = new ImportCSV();
        
        Component cpu1 = createCPU(1, "aeiou", "a", 14.5, 1.6, 23.4);
        
        Component cpu2 = createCPU(2, "banana", "b", 123.4, 25.4, 634.7);
        
        Component motherboard = createMotherboard(3, "abc", "c", 123, 32.09, 1245.6);
        
        Component cooler = createCooler(4, "qwerty", "d", 56.9, 12.0089, 123, 10);
        
        List<Component> expResult = new ArrayList();
        expResult.addAll(Arrays.asList(cpu1, cpu2, motherboard, cooler));
        
        List<Component> result = instance.importComponents(f);
        assertEquals(expResult, result);
    }

    private Component createCPU(int id, String model, String name, double condk, double convk, double radk) {
        CPU temp = new CPU();
        
        temp.setId(id);
        
        temp.setModel(model);
        
        temp.setName(name);
        
        temp.setCondK(condk);
        
        temp.setConvK(convk);
        
        temp.setRadk(radk);
        
        return temp;
    }

    private Component createMotherboard(int id, String model, String name, double condk, double convk, double radk) {
        Motherboard temp = new Motherboard();
        
        temp.setId(id);
        
        temp.setModel(model);
        
        temp.setName(name);
        
        temp.setCondK(condk);
        
        temp.setConvK(convk);
        
        temp.setRadk(radk);
        
        return temp;
    }
    
    private Component createCooler(int id, String model, String name, double condk, double convk, double radk, float constantArrivalTemp) {
        Cooler temp = new Cooler();
        
        temp.setId(id);
        
        temp.setModel(model);
        
        temp.setName(name);
        
        temp.setCondK(condk);
        
        temp.setConvK(convk);
        
        temp.setRadk(radk);
        
        temp.setConstantArrivalTemp(constantArrivalTemp);
        
        return temp;
    }
    
}
