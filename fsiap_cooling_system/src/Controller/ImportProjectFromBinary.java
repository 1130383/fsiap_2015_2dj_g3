/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import CoolingSystem.model.Project;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 *
 * @author Renato
 */
public class ImportProjectFromBinary {

    public static Project importFromBinary(File f) throws FileNotFoundException, IOException, ClassNotFoundException {
        Object cc;
        FileInputStream fileIn = new FileInputStream(f.getAbsolutePath());
        ObjectInputStream in = new ObjectInputStream(fileIn);
        cc = in.readObject();
        in.close();
        fileIn.close();
        return (Project) cc;
    }

}
