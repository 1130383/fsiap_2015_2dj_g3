/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import CoolingSystem.Resources.ExportToBinary;
import CoolingSystem.Resources.ExportToHtml;
import CoolingSystem.model.Project;
import GUI.Main_GUI;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author Renato
 */
public class ExportProject_controller {
    
    private Project mProject;
    private Main_GUI m_gui;
    private String m_path;
    
    public ExportProject_controller(Main_GUI gui, Project project)
    {   
        mProject = project;
        m_gui = gui;
        
    }
    
    public void exportToBinary() throws IOException{
        
        ExportToBinary export = new ExportToBinary();
        
        export.exportProject(mProject);
        
    }
    
    public void exportToHtml(String path) throws IOException{
        
        this.m_path=path;
        
        ExportToHtml export = new ExportToHtml();
        
        export.escreverDados(mProject, m_path);
        
  
    }
    
}
