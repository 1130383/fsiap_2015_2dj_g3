/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import CoolingSystem.Resources.ImportCSV;
import CoolingSystem.model.Component;
import CoolingSystem.model.Project;
import GUI.Main_GUI;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Marco
 */
public class ImportFile_controller {

    private Project mProject;
    private Main_GUI m_gui;

    public ImportFile_controller(Main_GUI gui, Project project) {
        mProject = project;
        m_gui = gui;
    }

    public void importFile() {
        JFileChooser chooser;
        FileNameExtensionFilter filter;
        int returnVal;
        File file;

        chooser = new JFileChooser();
        filter = new FileNameExtensionFilter("CSV Files", "csv");
        chooser.setFileFilter(filter);

        returnVal = chooser.showOpenDialog(m_gui);
        if (returnVal == JFileChooser.CANCEL_OPTION) {
            return;
        }

        file = chooser.getSelectedFile();
        try {

            ImportCSV importCSV;
            List<Component> components;

            importCSV = new ImportCSV();

            components = importCSV.importComponents(file);

            mProject.setStockList(components);

        } catch (IOException ex) {
            Logger.getLogger(Main_GUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        JOptionPane.showMessageDialog(m_gui, "Components carregados com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
    }

    public Project importProject(File f) throws IOException, FileNotFoundException, ClassNotFoundException {
        return ImportProjectFromBinary.importFromBinary(f);
    }

}
