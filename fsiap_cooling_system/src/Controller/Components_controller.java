/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import CoolingSystem.Resources.ImportCSV;
import CoolingSystem.model.CPU;
import CoolingSystem.model.Component;
import CoolingSystem.model.Cooler;
import CoolingSystem.model.Motherboard;
import CoolingSystem.model.Project;
import CoolingSystem.model.Tower;
import GUI.Simulation_GUI;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JLabel;

/**
 *
 * @author Andre
 */
public class Components_controller {

    private Project projeto;
    private List<CPU> cpu_list;
    private List<Cooler> cooler_list;
    private List<Motherboard> motherboard_list;
    private List<Tower> tower_list;
     

// TODO FALTA A COMPONENTE SALA

    public Components_controller(Project m_projeto) {
        projeto = m_projeto;
        cpu_list = new ArrayList<>();
        cooler_list = new ArrayList<>();
        motherboard_list = new ArrayList<>();
        tower_list = new ArrayList<>();  
        m_projeto.getComponentsList().clear();
        

    }
    
    public void getComponents()
    {
        formComponents();
    }

    public Components_controller() {
        projeto = new Project();
    }
    
    private void formComponents()
    {
        if(projeto.getStockList() == null){
            JOptionPane.showMessageDialog(null,"ERRO");
        }
        
        for (Component comp : projeto.getStockList()) {
             
            if (comp instanceof CPU) cpu_list.add((CPU)comp);
            
            if (comp instanceof Cooler) cooler_list.add((Cooler)comp);
            
            if (comp instanceof Motherboard) motherboard_list.add((Motherboard)comp);
            
            if (comp instanceof Tower) tower_list.add((Tower) comp);
            
        }
    }
    
    public void registerComponentsList(ArrayList componentes_selecionados)
    {  
        projeto.setComponentsList(componentes_selecionados);   
        
    }
    
    
    
    public List<CPU> getListaCpu(){
        return cpu_list;
    }
    public List<Cooler> getListaCooler(){
        return cooler_list;
    }
    public List<Motherboard> getListaMotherboard(){
        return motherboard_list;
    }
    public List<Tower> getListaTower(){
        return tower_list;
    }

    public Project getProject() {
        return projeto;
    }
    

}