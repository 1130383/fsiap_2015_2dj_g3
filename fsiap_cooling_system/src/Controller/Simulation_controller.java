/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import CoolingSystem.model.CPU;
import CoolingSystem.model.Component;
import CoolingSystem.model.Connection;
import CoolingSystem.model.Project;
import CoolingSystem.model.Statistic;
import java.util.List;

/**
 *
 * @author Marco
 */
public class Simulation_controller {
    
    private Project project;
    private int cpu_rate = 50;
    private int play_speed = 1;
    private List<Connection> connectionsList; 
    private CPU mCPU;
    private Statistic stats;
    
    public Simulation_controller(Project proj)
    {
        this.project = proj;
        proj.clean();
        connectionsList = proj.getConnectionsList();
        mCPU = findCPU();
    }
    
    public String [] getComponentsList()
    {
        String [] strList = new String [connectionsList.size()];
        int i = 0;
        for (Connection conn : connectionsList) {
            strList[i] = conn.getName();
            i++;
        }
        return strList;
    }
    
    private CPU findCPU()
    {
        for (Connection conn : connectionsList) {
            if (conn.getComponentA() instanceof CPU)
            {
                return (CPU)conn.getComponentA();
            }
            if (conn.getComponentB() instanceof CPU)
            {
                return (CPU)conn.getComponentB();
            }
        }
        
        return null;
    }
    
    public CPU getCPU()
    {
        return mCPU;
    }
    
    public String [] getTemperatureList()
    {
        String [] strList = new String [connectionsList.size()];
        int i = 0;
        for (Connection conn : connectionsList) {
            strList[i] = String.valueOf(Math.round(conn.getComponentA().getTemperature() * 1000.0) / 1000.0);
            i++;
        }
        return strList;
    }
    
    public void setCpuRate(int rate)
    {
        this.cpu_rate = rate;
       
        mCPU.setInstantUsage(rate);
            
    }
    
    
    public long getTimeInterval()
    {
        return project.getTimeInterval();
    }
    
    public void setPlaySpeed(int speed)
    {
        this.play_speed = speed;
        project.setPlaySpeed(play_speed);
    }    
    
    public void updateSystem()
    {
        if (project.getState() == Project.getSTATE_PLAYING())
        {
            project.update();
        }
    }
    
    public void setStopButtonClicked()
    {
        project.eraseElapsedTime();
        project.setState(Project.getSTATE_STOPPED());
        // TODO : Reset values for new simulation
    }
    
    public void setPlayPauseButtonClicked()
    {
        if (project.getState() == Project.getSTATE_PAUSED())
        {
            project.setState(Project.getSTATE_PLAYING());
        }else if (project.getState() == Project.getSTATE_PLAYING()){
            project.setState(Project.getSTATE_PAUSED());
        }else{
            project.setState(Project.getSTATE_PLAYING());
        }
        
    }   
    
    public boolean isPlaying()
    {
        return project.getState() == Project.getSTATE_PLAYING();
    }
    
    public boolean isPaused()
    {
        return project.getState() == Project.getSTATE_PAUSED();
    }
    
    public boolean isStopped()
    {
        return project.getState() == Project.getSTATE_STOPPED();
    }
}
