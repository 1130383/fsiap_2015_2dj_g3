/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Marco Class Project represents the Project
 */
public class Project implements Serializable {

	private static int STATE_STOPPED = 0;
	private static int STATE_PLAYING = 1; // when the experience is actually running
	private static int STATE_PAUSED = 2;

	private static double DEFAULT_SYSTEM_TEMP = 0.00; // default temperatue for all components
        private static int DefaultCPUStartingUsage = 40;
        
	public static int getSTATE_STOPPED() {
		return STATE_STOPPED;
	}

	public static int getSTATE_PLAYING() {
		return STATE_PLAYING;
	}

	public static int getSTATE_PAUSED() {
		return STATE_PAUSED;
	}
        
        private static int RandomInterval = 200;

	private int FPS = 16; // refresh rate, every 16 miliseconds (60 fps)
	private TimerTask timerTask;
	private Timer timer;
	private long elapsedTime; // total elapsed time of simulation
	private int playSpeed = 1; //
	private int state = STATE_STOPPED; // current state of simulation
        private int lastRandom = 0;
        

	private Statistic stats;

	private List<Component> componentsList; // list of selected components to run simulation
	private List<Connection> connectionsList;  // list of all connections (read from file) used in simulation
	private List<Component> stockList;  // all available components read from file

	public Project() {
		componentsList = new ArrayList<>();
		connectionsList = new ArrayList<>();
		stockList = new ArrayList<>();
		this.stats = new Statistic(connectionsList);
		timerTask = new SimulationTimerTask(this);

	}

	public TimerTask getTimerTask() {
		return this.timerTask;
	}

	public void newSimulationTimer() {
		this.timer = new Timer();
		timer.scheduleAtFixedRate(new SimulationTimerTask(this), 0, FPS);
	}

	public Timer getTimer() {
		return this.timer;
	}

	public int getFPS() {
		return FPS;
	}

	public void setFPS(int FPS) {
		this.FPS = FPS;
	}

	public void updateElapsedTime(long timeInterval) {
		elapsedTime += timeInterval;
	}

	public void eraseElapsedTime() {
		this.elapsedTime = 0;
	}

	public long getElapsedTime() {
		return this.elapsedTime;
	}

	public int getPlaySpeed() {
		return playSpeed;
	}

	public void setPlaySpeed(int playSpeed) {
		this.playSpeed = playSpeed;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
		if (state == STATE_PLAYING) {
			newSimulationTimer();
		} else {
			timer.cancel();
		}
	}

	public List<Component> getComponentsList() {
		return componentsList;
	}

	public void setComponentsList(List<Component> componentsList) {
		this.componentsList = componentsList;

		setConnectionsList();
	}

	public List<Connection> getConnectionsList() {
		return this.connectionsList;
	}

	/**
	 * Class knows a list of the components and wants to know what are the
	 * connections.
	 *
	 * @param connectionsList
	 */
	public void setConnectionsList() {

		/**
		 * Content init.
		 */
		Component cpu_trial = new CPU();
		Component mb_trial = new Motherboard();
		Component tower_trial = new Tower();
		Component cooler_trial = new Cooler();

		/**
		 * Reads all Components and creates the connections.
		 */
		for (int i = 0; i < this.componentsList.size(); i++) {

			if (this.componentsList.get(i).getIndex() == 1) {
				cpu_trial = this.componentsList.get(i);

			}
			if (this.componentsList.get(i).getIndex() == 2) {
				mb_trial = (Motherboard) this.componentsList.get(i);

			}
			if (this.componentsList.get(i).getIndex() == 3) {
				tower_trial = this.componentsList.get(i);

			}
			if (this.componentsList.get(i).getIndex() == 4) {
				cooler_trial = (Cooler) this.componentsList.get(i);
			}
		}

		/**
		 * Convencional Connections.
		 *
		 */
		Connection con_1 = new Connection("cpu_to_mb", mb_trial, cpu_trial);
		Connection con_2 = new Connection("cpu_to_tower", tower_trial, cpu_trial);
		Connection con_3 = new Connection("cpu_to_cooler", cpu_trial, cooler_trial);
                this.connectionsList.clear();
		this.connectionsList.add(con_1);
		this.connectionsList.add(con_2);
		this.connectionsList.add(con_3);
	}

	public List<Component> getStockList() {
		return stockList;
	}

	public void setStockList(List<Component> stockList) {
		this.stockList = stockList;
	}

	public void run() {
		setState(STATE_PLAYING);
	}

	/**
	 * this method will by Use Case 2 every x miliseconds
	 */
	public void update() {
		if (getState() == STATE_PLAYING) {
			updateElapsedTime(getTimeInterval());

			int divideTime = connectionsList.size(); // divide timeInterval per connections number

			double transfer;
			for (Connection conn : connectionsList) {
                                
                            if (lastRandom > RandomInterval / playSpeed)
                            {
                                if (conn.getComponentA() instanceof CPU)
                                {
                                    ((CPU)conn.getComponentA()).randomUsage();
                                }
                                if (conn.getComponentB() instanceof CPU)
                                {
                                    ((CPU)conn.getComponentB()).randomUsage();
                                }
                                lastRandom = 0;
                            }else{ lastRandom ++;}
                            
                            getStats().setElapsedTime(elapsedTime);
                            
                            transfer = conn.transfer((double) this.FPS / divideTime);
                            conn.setCurrentTransferRate(transfer);

                            for (StatsItem statsItem : getStats().getItemList()) {
                                    statsItem.update();
                            }
			}

		}
	}

	public long getTimeInterval() {
		return playSpeed * FPS;
	}

	public void pause() {
		setState(STATE_PAUSED);
	}

	public void stop() {
		setState(STATE_STOPPED);
	}

	/**
	 * restart values for new simulation
	 */
	public void clean() {
		this.eraseElapsedTime();
                playSpeed = 1;
		// TODO: clean() not fully implemented yet

		/* experimental values
		 if (componentsList.size() == 0)
		 {
		 Component comp = new CPU();
		 comp.setName("CPU Intel IA32");
		 componentsList.add(comp);
		 comp = new Motherboard();
		 comp.setName("Motherboard ASUS XPTO");
		 componentsList.add(comp);
		 comp = new Tower();
		 comp.setName("Tower Desktop 3000");
		 componentsList.add(comp);
		 comp = new Room();
		 comp.setName("Sala H311");
		 componentsList.add(comp);

		 }
		 end of experimental values (TODO : delete later)*/
		this.stats = new Statistic(connectionsList);

		// here reset all values
		for (Component component : componentsList) {
			component.setTemperature(DEFAULT_SYSTEM_TEMP);
                        if (component instanceof CPU)
                        {
                            ((CPU)component).setInstantUsage(DefaultCPUStartingUsage);
                        }
		}
	}

    /**
     * @return the stats
     */
    public Statistic getStats() {
        return stats;
    }

}
