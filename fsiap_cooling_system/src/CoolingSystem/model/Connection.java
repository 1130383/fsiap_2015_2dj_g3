/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.model;

import CoolingSystem.Resources.Resources;
import java.io.Serializable;

/**
 *
 * @author Pedro Gomes<1130383@isep.ipp.pt>
 */
public class Connection implements Serializable {

	/**
	 * Componentes.
	 */
	private Component component_1;
	private Component component_2;

	/**
	 * current value of (W) transfered beetween components.
	 */
	private double currentTransferRate;

	/**
	 * Connection name.
	 */
	private String name;

	/**
	 * Connection type.
	 */
	private int type;

	/**
	 *
	 * @param name
	 * @param x
	 * @param y
	 */
	public Connection(String name, Component x, Component y) {

		this.name = name;
		this.component_1 = x;
		this.component_2 = y;
		this.type = ConnectionType();
	}

	/**
	 * Empty Constructor.
	 */
	public Connection() {

	}

	/**
	 *
	 * @return Connection Name.
	 */
	public String getName() {
		return this.name + "# " + component_1.getModel()
			+ "# " + component_2.getModel();
	}

	/**
	 *
	 * @return Connection ID.
	 */
	private int ConnectionType() {

		if (this.component_1.getIndex() == Resources.tower_index || this.component_2.
			getIndex() == Resources.tower_index) {

			return 2;
		}

		return 1;
	}

	/**
	 *
	 * @return first component of this connection
	 */
	public Component getComponentA() {
		return this.component_1;
	}

	/**
	 *
	 * @return second component of this connection
	 */
	public Component getComponentB() {
		return this.component_2;
	}

	/**
	 * calculates the amount of heat transfered between 2 components based on
	 * time interval (in seconds)
	 *
	 * @param timeInterval
	 * @return
	 */
	public double transfer(double timeInterval) {

		double flux = 0;

		if (this.type == Resources.Conduction_id) {

			flux = ConductionCalculus();

		} else if (this.type == Resources.Convection_id) {

			flux = ConvectionCalculus();

		}

		return flux * timeInterval;

	}

	/**
	 *
	 * @return flux
	 */
	public double ConvectionCalculus() {
		double result;

		CPU cpu_temp = new CPU();

		if (this.component_1.getIndex() == Resources.cpu_index) {
			cpu_temp = (CPU) this.component_1;

		} else if (this.component_2.getIndex() == Resources.cpu_index) {
			cpu_temp = (CPU) this.component_2;
		}

		result = Resources.fluid_coefficient * Resources.cpu_area * (cpu_temp.
			getInstantUsage() - Resources.tower_temperature);

		if (result < 0) {
			return (-1) * result;
		} else {
                return result;
		}

	}

	/**
	 * Determina o fluxo de calor entre determinados componentes a diferentes
	 *
	 * @return flux
	 */
	public double ConductionCalculus() {

		CPU cpu_temp = new CPU();

		double result;

		int flag = 0;

		if (this.component_1.getIndex() == Resources.cpu_index) {
			cpu_temp = (CPU) this.component_1;

		} else if (this.component_2.getIndex() == Resources.cpu_index) {
			cpu_temp = (CPU) this.component_2;
		}

		if (this.component_1.getIndex() == Resources.cooler_index || this.component_2.
			getIndex() == Resources.cooler_index) {
			flag = Resources.cooler_index;
		}

		if (this.component_1.getIndex() == Resources.mb_index || this.component_2.
			getIndex() == Resources.mb_index) {
			flag = Resources.mb_index;
		}

		if (flag == Resources.cooler_index) {

			result = Resources.cpu_conductivity * Resources.cpu_area * ((cpu_temp.
				getInstantUsage() - Resources.cooler_temperature) / Resources.cpu_size);
		} else {

			result = Resources.cpu_conductivity * Resources.cpu_area * ((cpu_temp.
				getInstantUsage() - Resources.mb_temperature) / Resources.cpu_size);
		}

		if (result < 0) {
			return (-1) * result;
		} else {
                return result;
	}
	}

	/**
	 *
	 * @return
	 */
	public double getCurrentTransferRate() {

		return currentTransferRate;

	}

	/**
	 *
	 * @param currentTransferRate
	 */
	public void setCurrentTransferRate(double currentTransferRate) {

		this.currentTransferRate = currentTransferRate;
		component_1.setTemperature(currentTransferRate);
//                if (component_1 instanceof CPU)
//                {
//                    component_1.setTemperature(currentTransferRate);
//                }else{
//                    component_1.setTemperature(currentTransferRate *-1);
//                }
//
//                if (component_2 instanceof CPU)
//                {
//                    component_2.setTemperature(currentTransferRate);
//                }else{
//                    component_2.setTemperature(currentTransferRate *-1);
//                }

	}

	/**
	 * Static Methods.
	 */
	/**
	 * Check if Component is a CPU.
	 *
	 * @param component_1
	 * @return boolean.
	 */
	public static boolean checkCPU(Component component_1) {

		boolean flag = false;
		if (component_1 instanceof CPU) {
			flag = true;
		}
		return flag;
	}

	/**
	 * Check if component is a Motherboard.
	 *
	 * @param component
	 * @return booelan
	 */
	public static boolean checkMotherboard(Component component) {
		boolean flag = false;
		if (component instanceof Motherboard) {
			flag = true;
		}
		return flag;
	}

	/**
	 * Check if component is a Cooler.
	 *
	 * @param component
	 * @return
	 */
	public static boolean checkCooler(Component component) {

		boolean flag = false;
		if (component instanceof Cooler) {
			flag = true;
		}
		return flag;

	}

	/**
	 * Check if component is a Tower.
	 *
	 * @param component
	 * @return
	 */
	public static boolean checkTower(Component component) {
		boolean flag = false;
		if (component instanceof Tower) {
			flag = true;
		}
		return flag;

	}

	@Override
	public String toString() {
		return "Connection name: " + this.name + "; Connection Type: " + this.type + "; Component_A: "
			+ this.component_1.getModel() + "; Component_B: " + this.component_2.
			getModel();

	}

}
