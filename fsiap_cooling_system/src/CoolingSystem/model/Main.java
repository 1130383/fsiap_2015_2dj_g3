/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.model;

import CoolingSystem.Resources.ImportCSV;
import GUI.LanguageChooser_GUI;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author Pedro Gomes <1130383@isep.ipp.pt>
 */
public class Main {
        
        private static Project project;
        
        public static LanguageSelector lang;
        public static String DefaultFilePath = "teste2.csv";

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
            
            project = new Project();
            lang = new LanguageSelector();
            new LanguageChooser_GUI(project); 
            try{
                project.setStockList(new ImportCSV().importComponents(new File(DefaultFilePath)));
            }catch(IOException ex){};
	}

}
