/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * this class is used to store stats from simulation
 *
 * @author Marco
 */
public class Statistic implements Serializable {

    private long elapsedTime;    // total elapsedtime of simulation
    private List<StatsItem> itemList;  // item of the stats (each item represents one component)
    private long nIterations = 0;

    public Statistic(List<Connection> connList) {
        itemList = new ArrayList<>();
        for (Connection conn : connList) {
            itemList.add(new StatsItem(conn, this));
        }
    }

    public void setElapsedTime(long elapsed) {
        this.elapsedTime = elapsed;
    }

    public List<StatsItem> getItemList() {
        return itemList;
    }

    public void setItemList(List<StatsItem> itemList) {
        this.itemList = itemList;
    }
    
    public long getElapsedTime()
    {
        return this.elapsedTime;
    }
    
    public void increasenumberofIterations()
    {
        nIterations++;
    }

}
