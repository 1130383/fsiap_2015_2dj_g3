/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.model;

import java.io.Serializable;

/**
 * Que classe é esta?
 *
 * @author Marco
 */
public class Room implements Serializable {

	private float volume; // volume area in m3

	public Room() {

	}

	public float getVolume() {
		return volume;
	}

	public void setVolume(float volume) {
		this.volume = volume;
	}

}
