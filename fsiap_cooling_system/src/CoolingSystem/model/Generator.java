/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoolingSystem.model;

/**
 * This interface represents all components who produce heat to itself based on ist's usage (0 to 100%)
 * @author Marco
 */
public interface Generator {
  
    public void generate (double timeInterval);
}
