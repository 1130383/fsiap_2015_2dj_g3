/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.model;

import CoolingSystem.Resources.Resources;
import java.io.Serializable;

/**
 *
 * @author Pedro Gomes<1130383@isep.ipp.pt>
 */
public class Motherboard extends Component implements Serializable {

	private double Area;

	public Motherboard(double area) {
		this.Area = area;
	}

	public Motherboard() {

	}

	@Override
	public String toString() {
		return "Motherboard: " + super.toString();
	}

	public double getArea() {
		return this.Area;
	}

	public void setArea(double aArea) {
		this.Area = aArea;
	}

	@Override
	public void setTemperature(double temperature) {
		super.setTemperature(temperature);
	}

	@Override
	public double getTemperature() {
		return super.getTemperature();
	}

	@Override
	public void setRadk(double radk) {
		super.setRadk(radk);
	}

	@Override
	public double getRadk() {
		return super.getRadk();
	}

	@Override
	public void setConvK(double convK) {
		super.setConvK(convK);
	}

	@Override
	public double getConvK() {
		return super.getConvK();
	}

	@Override
	public void setCondK(double condK) {
		super.setCondK(condK);
	}

	@Override
	public double getCondK() {
		return super.getCondK();
	}

	@Override
	public void setName(String name) {
		super.setName(name);
	}

	@Override
	public String getName() {
		return super.getName();
	}

	@Override
	public void setModel(String model) {
		super.setModel(model);
	}

	@Override
	public String getModel() {
		return super.getModel();
	}

	@Override
	public void setId(int id) {
		super.setId(id);
	}

	@Override
	public int getId() {
		return super.getId();
	}

	@Override
	public int getIndex() {
		return Resources.mb_index;
	}

}
