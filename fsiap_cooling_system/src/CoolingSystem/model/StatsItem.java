/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.model;

import java.io.Serializable;

/**
 * This is an Item of Statistics. Each Item represents a component
 *
 * @author Marco
 */
public class StatsItem implements Serializable {

    private Connection connection; // the component of this item
    private double minTranseferRate; // the lowest temperature of the component in simulation
    private double maxTranseferRate; // the highest temperature of the component in simulation
    private String name;
    private double totalRateTransfered =0;
    private Statistic parent;
    

    public StatsItem(Connection conn, Statistic parent) {
        this.connection = conn;
        this.name = conn.getName();
        this.parent = parent;

    }
    
    public double getTotalTransferedRate()
    {
        return totalRateTransfered;
    }
    
    public String getName() {
        return this.name;
    }

    public double getVariation() {
        return Math.abs(maxTranseferRate - minTranseferRate);
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public double getMinTranseferRate() {
        return minTranseferRate;
    }

    public void setMinTranseferRate(float minTranseferRate) {
        this.minTranseferRate = minTranseferRate;
    }

    public double getMaxTranseferRate() {
        return maxTranseferRate;
    }
 
    public void setMaxTranseferRate(float maxTranseferRate) {
        this.maxTranseferRate = maxTranseferRate;
    }

    public void update() {
        
        if (getConnection().getComponentA().getTemperature() < minTranseferRate) {
            minTranseferRate = getConnection().getComponentA().getTemperature();
        }
        if (getConnection().getComponentA().getTemperature() > maxTranseferRate) {
            maxTranseferRate = getConnection().getComponentA().getTemperature();
        }
        totalRateTransfered += getConnection().getComponentA().getTemperature();
    }
    
    public double getAverage()
    {
        if (parent.getElapsedTime() > 0)
        {
            return totalRateTransfered / parent.getElapsedTime();
        }
        return 0;
    }

}
