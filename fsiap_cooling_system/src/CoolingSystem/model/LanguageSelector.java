/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.model;

/**
 *
 * @author Marco
 */
public class LanguageSelector {

	public static int PT = 0;
	public static int EN = 1;
	//---------------------MAIN_GUI------------------------//
	public static String[] Main_File_ImportFile = {"Importar de Ficheiro", "Import from File"};
	public static String[] Main_File_RunSimulation = {"Correr Simulação", "Run Simulation"};
	public static String[] Main_File_ExportResults = {"Exportar Resultados", "Export Results"};
	public static String[] Main_File_SaveData = {"Guardar Dados", "Save Data"};
	public static String[] Main_File_File = {"Ficheiro", "File"};
	public static String[] Main_File_Edit = {"Editar", "Edit"};
	public static String[] Main_File_LoadProject = {"Carregar Projeto", "Load Project"};
	public static String[] Main_File_JOptionPane = {"Ups algo correu mal !", "Ups Something went wrong!"};
	public static String[] Main_File_JOptionPane2 = {"Specify a file to save", "Especificar onde guardar"};
	//---------------------COMPONENTS_GUI-----------------------//
	public static String[] Components_GUI_Processor = {"Processador", "Processor"};
	public static String[] Components_GUI_Cooler = {"Sistema Refrigeração", "Cooling System"};
	public static String[] Components_GUI_MotherBoard = {"Placa Mãe", "Motherboard"};
	public static String[] Components_GUI_Tower = {"Torre de Sistema", "System Tower"};
	public static String[] Components_GUI_Button = {"Gerar Sistema", "Create System"};
	public static String[] Components_GUI_JOptionPane1 = {"Os dados serão inseridos na simulação tem a certeza que pretende continuar?", "The data will be saved and inserted will you like to proceed ?",};
	public static String[] Components_GUI_JOptionPane2 = {"Por favor verifique se os dados inseridos estao corretos", "Please check the data from the checklist"};
	public static String[] Components_GUI_Error = {"Erro", "Error"};
	//-----------------------------SIMULATION_GUI------------------------//
	public static String[] Simulation_GUI_Title = {"Correr Simulação", "Run Simulation"};
	public static String[] Simulation_GUI_JOptionPane1 = {"Tem a certeza que pretende iniciar uma nova simulação (dados anteriores serão perdidos)", "Are you sure you want to start a new simulation? (all previous stats will be lost)"};
	public static String[] Simulation_GUI_JOptionPane2 = {"Alerta nova simulação", "New Simulation Alert"};
	public static String[] Simulation_GUI_Button3 = {"Voltar", "Return"};
	public static String[] Simulation_GUI_Button2 = {"Iniciar", "Start"};
	public static String[] Simulation_GUI_Button1 = {"Pausa", "Pause"};
	public static String[] Simulation_GUI_JLabel9 = {"Velocidade", "Speed"};
	public static String[] Simulation_GUI_JLabel7 = {"Utilização CPU (%)", "CPU usage (%)"};
	public static String[] Simulation_GUI_JLabel5 = {"Tempo Decorrido", "Elapsed Time"};
	public static String[] Simulation_GUI_JLabel2 = {"Sistema", "System"};
	public static String[] Simulation_GUI_JLabel4 = {"Fluxo de Sistema (W)", "System Flow (W)"};
	//--------------------------SIMULATION_GUI STATES LINE 382----------------//
	public static String[] Simulation_GUI_Pause = {"Pausa", "Pause"};
	public static String[] Simulation_GUI_Stop = {"Parar", "Stop"};
	public static String[] Simulation_GUI_Continue = {"Continuar", "Continue"};
	public static String[] Simulation_GUI_Start = {"Iniciar", "Start"};

	private int language;

	public LanguageSelector() {
		this.language = PT;
	}

	public void setLanguage(int lang) {
		this.language = lang;
	}

	public int getLanguage() {
		return this.language;
	}

}
