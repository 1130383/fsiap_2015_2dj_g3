/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.model;

import java.io.Serializable;

/**
 *
 * @author Marco
 */
public abstract class Component implements Serializable {

    private int id;
    private String model;
    private String name;
    private double condK;
    private double convK;
    private double radk;
    private double temperature;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCondK() {
        return condK;
    }

    public void setCondK(double condK) {
        this.condK = condK;
    }

    public double getConvK() {
        return convK;
    }

    public void setConvK(double convK) {
        this.convK = convK;
    }

    public double getRadk() {
        return radk;
    }

    public void setRadk(double radk) {
        this.radk = radk;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    /**
     * getIndex for type of component.
     *
     * @return
     */
    abstract public int getIndex();

    @Override
    public String toString() {
        return getModel();
    }

}
