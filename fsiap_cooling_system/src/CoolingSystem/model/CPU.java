/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.model;

import CoolingSystem.Resources.Resources;
import java.io.Serializable;
import java.util.Random;

/**
 *
 * @author Pedro Gomes<1130383@isep.ipp.pt>
 *
 */
public class CPU extends Component implements Serializable, Generator {

	private static double size;
	private static double conductivity;
        private static int MaxRandomUsage = 5;
        private static int MinRandomUsage = -5;
        private static int MaxUsage = 85;
        private static int MinUsage = 30;

	/**
	 * Área do CPU (varia entre o modelo).
	 */
	private double area = Resources.cpu_area;
	/**
	 * Temperatura máxima do CPU pode atingir.
	 */
	private double temp_max;

	private double heat_production; // The amount (in ºC) of heat produced by second at 100%usage

	private int instant_usage = 0;

	/**
	 * Empty
	 */
	public CPU() {

	}

	/**
	 * Construtor CPU.
	 *
	 * @param area
	 */
	public CPU(double area) {
		this.area = area;
	}

	@Override
	public String toString() {
		return "CPU: " + super.toString();
	}

	/**
	 * @return the area.
	 */
	public double getArea() {
		return area;
	}

	/**
	 * unused method.
	 *
	 * @param area the area to set.
	 */
	private void setArea(double area) {
		this.area = area;
	}

	/**
	 * @return the temp_max
	 */
	public double getTemp_max() {
		return temp_max;
	}

	/**
	 * Unused method;
	 *
	 * @param temp_max the temp_max to set
	 */
	private void setTemp_max(double temp_max) {
		this.temp_max = temp_max;
	}

	/**
	 *
	 * @param usage the instant CPU usage (0-100%)
	 */
	public void setInstantUsage(int usage) {
		this.instant_usage = usage;
	}

	/**
	 *
	 * @return instant cpu usage
	 */
	public int getInstantUsage() {
		return this.instant_usage;
	}
        
        /**
         * random new cpu usage on every system update
         */
        public void randomUsage()
        {
            Random r  = new Random();
            instant_usage += r.nextInt((MaxRandomUsage - MinRandomUsage) + 1) + MinRandomUsage;
            if (instant_usage > MaxUsage) instant_usage = MaxUsage;
            if (instant_usage < MinUsage) instant_usage = MinUsage;
        }

	/**
	 * calculates the amount of auto-incresaing temperature, per second, based
	 * on unstant usage
	 */
	@Override
	public void generate(double timeInterval) {

		// TODO : implement generate physics
	}

	@Override
	public int getIndex() {

		return Resources.cpu_index;
	}

}
