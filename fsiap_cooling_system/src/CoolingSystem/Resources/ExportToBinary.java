/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.Resources;

import CoolingSystem.model.Project;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Renato
 */
public class ExportToBinary implements Export {

    @Override
    public void exportProject(Project project) throws IOException {
        FileOutputStream fileOut = new FileOutputStream(new File("project.ser"));
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(project);
        out.close();
        fileOut.close();
    }

}
