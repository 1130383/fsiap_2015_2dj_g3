/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.Resources;

import CoolingSystem.model.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * Classe responsável pela importação através de ficheiro CSV de mais
 * componentes para configuração do sistema.
 *
 * @author 1120681@isep.ipp.pt
 */
public class ImportCSV implements Import {

    @Override
    public List<Component> importComponents(File f) throws FileNotFoundException, IOException {

        BufferedReader in;
        FileReader reader;
        List<Component> newComponents;
        Map<String, Integer> mapaValores;
        String line;
        String firstLine[];
        int nColumns;
        int i;

        reader = new FileReader(f);
        in = new BufferedReader(reader);

        newComponents = new ArrayList();

        line = in.readLine();

        firstLine = line.split(";");

        mapaValores = new HashMap();

        nColumns = firstLine.length;

        for (i = 0; i < nColumns; i++) {
            mapaValores.put(firstLine[i].toLowerCase(), i);
        }

        while ((line = in.readLine()) != null) {
            Component newComponent = filterLine(line.split(";"), mapaValores);
            newComponents.add(newComponent);
        }
        in.close();
        return newComponents;
    }

    private Component filterLine(String line[], Map<String, Integer> mapaValores) {
        Component newComponent = null;
        int positionType;
        String componentType;

        positionType = mapaValores.get("type");
        componentType = line[positionType].toLowerCase();

        switch (componentType) {
            case "cpu":
                newComponent = createCPUFromLine(line, mapaValores);
                break;
            case "motherboard":
                newComponent = createMotherboardFromLine(line, mapaValores);
                break;
            case "cooler":
                newComponent = createCoolerFromLine(line, mapaValores);
                break;
            case "tower":
                newComponent = createCaseFromLine(line, mapaValores);
            default:
                break;
        }

        return newComponent;
    }

    private Component createCPUFromLine(String[] line, Map<String, Integer> mapaValores) {
        CPU cpu;

        cpu = new CPU();

        cpu.setCondK(Double.parseDouble(line[mapaValores.get("condk")]));
        
        cpu.setConvK(Double.parseDouble(line[mapaValores.get("convk")]));
        
        cpu.setId(Integer.parseInt(line[mapaValores.get("id")]));
        
        cpu.setModel(line[mapaValores.get("model")]);
        
        cpu.setName(line[mapaValores.get("name")]);
        
        cpu.setRadk(Double.parseDouble(line[mapaValores.get("radk")]));

        return cpu;
    }

    private Component createMotherboardFromLine(String[] line, Map<String, Integer> mapaValores) {
        Motherboard motherboard;
        
        motherboard = new Motherboard();
        
        motherboard.setCondK(Double.parseDouble(line[mapaValores.get("condk")]));
        
        motherboard.setConvK(Double.parseDouble(line[mapaValores.get("convk")]));
        
        motherboard.setId(Integer.parseInt(line[mapaValores.get("id")]));
        
        motherboard.setModel(line[mapaValores.get("model")]);
        
        motherboard.setName(line[mapaValores.get("name")]);
        
        motherboard.setRadk(Double.parseDouble(line[mapaValores.get("radk")]));
        
        return motherboard;
    }

    private Component createCoolerFromLine(String[] line, Map<String, Integer> mapaValores) {
        Cooler cooler;
        
        cooler = new Cooler();
        
        cooler.setCondK(Double.parseDouble(line[mapaValores.get("condk")]));
        
        cooler.setConvK(Double.parseDouble(line[mapaValores.get("convk")]));
        
        cooler.setId(Integer.parseInt(line[mapaValores.get("id")]));
        
        cooler.setModel(line[mapaValores.get("model")]);
        
        cooler.setName(line[mapaValores.get("name")]);
        
        cooler.setRadk(Double.parseDouble(line[mapaValores.get("radk")]));
        
        cooler.setConstantArrivalTemp(Float.parseFloat(line[mapaValores.get("constantarrivaltemp")]));
        
        return cooler;
    }

    private Component createCaseFromLine(String[] line, Map<String, Integer> mapaValores) {
        Tower tower;
        
        tower = new Tower();
        
        tower.setCondK(Double.parseDouble(line[mapaValores.get("condk")]));
        
        tower.setConvK(Double.parseDouble(line[mapaValores.get("convk")]));
        
        tower.setId(Integer.parseInt(line[mapaValores.get("id")]));
        
        tower.setModel(line[mapaValores.get("model")]);
        
        tower.setName(line[mapaValores.get("name")]);
        
        tower.setRadk(Double.parseDouble(line[mapaValores.get("radk")]));
        
        tower.setBaseHeight(Double.parseDouble(line[mapaValores.get("baseheight")]));
        
        tower.setBaseWidth(Double.parseDouble(line[mapaValores.get("basewidth")]));
        
        tower.setSideHeight(Double.parseDouble(line[mapaValores.get("sideheight")]));
        
        tower.setHeatExtractionRate(Double.parseDouble(line[mapaValores.get("heatextractionrate")]));
        
        return tower;
    }

}
