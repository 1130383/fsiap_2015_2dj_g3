/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.Resources;

import CoolingSystem.model.Component;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Renato
 */
public interface Import {
    
    public List<Component> importComponents(File f) throws FileNotFoundException, IOException;
    
}
