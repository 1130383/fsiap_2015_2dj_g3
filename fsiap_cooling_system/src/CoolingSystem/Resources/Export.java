/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.Resources;

import CoolingSystem.model.Project;
import java.io.IOException;

/**
 *
 * @author Renato
 */
public interface Export {
    
    public void exportProject(Project project) throws IOException;
    
}
