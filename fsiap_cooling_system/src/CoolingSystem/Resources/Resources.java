/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.Resources;

/**
 *
 * @author Pedro Gomes<1130383@isep.ipp.pt>
 *
 */
public class Resources {

	/**
	 * indexes for all tested components.
	 */
	public final static int cpu_index = 1;
	public final static int mb_index = 2;
	public final static int tower_index = 3;
	public final static int cooler_index = 4;

	/**
	 * Connection Types.
	 */
	public final static int Conduction_id = 1;
	public final static int Convection_id = 2;

	/**
	 * 5mm standart.
	 *
	 */
	public static double cpu_size = 0.005;

	/**
	 * Condutividade térmica do CPU (condutividade térmica do cobre =
	 * 398(W/m*C).
	 *
	 */
	public static double cpu_conductivity = 401;

	/**
	 * Coeficiente térmico do Ar - W m-1 C-1
	 */
	public static double fluid_coefficient = 0.025;

	/**
	 * Temperature in Celsius.
	 */
	public static double cooler_temperature = 20;

	public static double mb_temperature = 30;

	public static double tower_temperature = 25;

	public static double cpu_area = 0.005; //m2

}
