/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CoolingSystem.Resources;

import CoolingSystem.model.Project;
import CoolingSystem.model.StatsItem;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author mac
 */
public class ExportToHtml {

	/**
	 *
	 * @return @throws FileNotFoundException
	 * @throws IOException
	 */
	public String abrirHtml() throws FileNotFoundException, IOException {

		String out = ("<!DOCTYPE html>"
			+ "<html><head><title>Estatisticas</title>"
			+ "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>"
			+ "<link rel='icon' type='image/ico' href='http://www.isep.ipp.pt/favicon.ico'></head>"
			+ "<body > <div><center><img src=\'http://www.dei.isep.ipp.pt/images/topo_index.png\' alt=\'Logotipo ISEP\'></center></div>");
		return out;
	}

	public String fecharHtml() {
		String out;
		out = ("</body></html>");
		return out;
	}

	public void abrirPagHtml(String args) throws IOException, URISyntaxException {

		String dir = new File(".\\" + args).getCanonicalPath();
		dir = dir.replace("\\", "/");
		Desktop d = Desktop.getDesktop();
		try {
			d.browse(new URI("File:///" + dir));
		} catch (IOException e) {
			System.out.println(e);
		} catch (URISyntaxException e) {
			System.out.println(e);
		}

	}

	public void escreverDados(Project project, String path) throws FileNotFoundException, IOException {

		File arq = new File(path + ".html");

		try {
			arq.createNewFile();
			//FileWriter fileWriter = new FileWriter(arq,false);
			PrintWriter printWriter = new PrintWriter(arq.getAbsolutePath(), "UTF-8");

			DecimalFormat df = new DecimalFormat("###.##");
			DecimalFormat s_note = new DecimalFormat("000E00");

			df.setRoundingMode(RoundingMode.UP);
			String out;

			long elapsed = project.getStats().getElapsedTime();
			String totaltime = String.
				format("%02d:%02d:%02d:%03d", TimeUnit.MILLISECONDS.
					   toHours(elapsed),
					   TimeUnit.MILLISECONDS.toMinutes(elapsed) - TimeUnit.HOURS.
					   toMinutes(TimeUnit.MILLISECONDS.toHours(elapsed)),
					   TimeUnit.MILLISECONDS.toSeconds(elapsed) - TimeUnit.MINUTES.
					   toSeconds(TimeUnit.MILLISECONDS.toMinutes(elapsed)),
					   TimeUnit.MILLISECONDS.toMillis(elapsed) - TimeUnit.SECONDS.
					   toMillis(TimeUnit.MILLISECONDS.toSeconds(elapsed)));

			out = abrirHtml();

			out += ("<table border=\"1\"");
			out += ("<tr>");
			out += ("<th> Simulation time </th>");
			out += ("<td>" + totaltime + "</td>");
			out += ("</table>");

			out += ("<table border=\"1\"");
			out += ("<tr>");
			out += ("<th> Item </th>");
			out += ("<th> MinTransferRate (W) </th>");
			out += ("<th> Variation (W) </th>");
			out += ("<th> MaxTransferRate (W) </th>");
			out += ("<th> Average (W) </th>");

			for (StatsItem itemList : project.getStats().getItemList()) {

				out += ("<tr>");
				//out += ("<th>     </th>");
				out += ("<td>" + itemList.getName() + "</td>");
				out += ("<td>" + String.valueOf(s_note.format(itemList.
					getMinTranseferRate())) + "</td>");
				out += ("<td>" + String.valueOf(s_note.format(itemList.
					getVariation())) + "</td>");
				out += ("<td>" + String.valueOf(s_note.format(itemList.
					getMaxTranseferRate())) + "</td>");
				out += ("<td>" + String.valueOf(s_note.format(itemList.
					getAverage())) + "</td>");
				out += ("</tr>");
			}

			out += ("</table>");
			out += fecharHtml();

			printWriter.print(out);
			printWriter.flush();
			printWriter.close();

			//JOptionPane.showMessageDialog(null,"Gravado com sucesso");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
